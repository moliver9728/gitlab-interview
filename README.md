## Welcome!

I created this personal README to help you learn more about me and my working style. Consider it a living document and please do share feedback on it, especially if information that is interesing to you, is missing. 


- [My Linkedin Profile](https://www.linkedin.com/in/michaeleoliver/)


## Hello, I'm Michael 👋

- I was born and raised in [Central Coast California](https://www.visitcalifornia.com/region/central-coast/), before going to CSU Long Beach for two years. After that, I moved to the DC area to persue a Masters in [Human Factors and Applied Congition](https://humanfactors.gmu.edu/about/the-program) at George Mason University. Currently, I am still living in the DC area but may be moving towards the US west coast in the next few years.
- In my free time I am often watching F1 or NFL, attempting to draw, playing strategy games, reading, or traveling with my partner.
    - I often spend my free time exploring side projects in data science or statistics, trying to learn more about various fields, or looking into public data sets like the [US Census Bureau](https://data.census.gov/cedsci/).
    - I do eventually hope to get into more creative hobbies like painting or clay modeling, but I tend to procrastinate on such things.
- I shy away from most social media and live a pretty introverted life (which is helped by Gitlab's culture), prefering to hang out with my partner and dog, Tilly.


## My role

I am a UX Researcher for Secure and Protect. I joined on Aug, 16th 2021, and the [job description](https://about.gitlab.com/job-families/engineering/ux-researcher/#intermediate) is probably more specific about my role than me. What I like to highlight though is my desire in spreading research and data based insights to my stakeholders and teams. I love to learn more about technology I'm not familiar with, and always enjoy diving into data on various topics. 


## How I work

* **I am based in EST and like to start around 9 am**. However, mornings are not always my favorite and I tend to work late, ocassionally until 7-8. To keep my strees manageable, I may start some mornings later so I can take a walk with my dog.
- I have a disability that occasionally makes it challenging to stay seated for long periods of times, but so far it has rarely complicated work thankfully.
- My strategy for problem solving is to have a well thought out processes or logic, and then trying to create efficiencies in that plan.
   - I try to be very aware and appreciative of other people's time and hope that they do the same for me.

## Working with me

- I assume positive intent.
- I try to help when possible.
- I Think simple, direct feedback is the easiest way to communicate.
- I do not usually do strict deadlines except when necessary. Instead, I have an [Epic - &7900](https://gitlab.com/groups/gitlab-org/-/epics/7900) that tracks all of the research projects I may be working on. You can see the priority and status of each project and go into those research issues in order to get an estimate on time to completion.

## Want help on research?
  - I totally understand if you don't know all of the answers to the questions, but what I absolutely need from you is the Background & Context, Overarching Goals, and any relevant past work or research done. Every project is different and these things help me understand yours.
- @ Mention me directly in your issue to start the conversation. 
  - If you need me to look over your plan, review your research questions, or help fill  knowledge gaps in possible methods, then just let me know your problem as a comment in your issue and I will get back within a few days most likely. 
  - If you have a more complex situations like folding difficult research into your roadmap, having too many outstanding questions in your plan, or not knowing what type of research study/method to conduct then you can send a meeting on my calendar (if you're in a timezone that works) or send a slack message to start a more synced conversation. 
    - After we establish more context to the issue, we can then use comments to further the discussion or updates. 
- If I don't get back to you within a day or two then it's likely because I am in the middle of data collection / synthesis for a major project, and I apologize. Feel free to slack me if you have anything more pressing and I will get back as soon as I can.


## Strengths and quirks

* **I am passionate and stronger in quantitative research methods**. However, I regularly preform user interviews and other qualitative methods.
- **I like managing large data**  and trying to find the patterns in chaos.
- **I love being efficient.**
- **I like learning new skills** and will be happy to try to pick up a new tool if it means solving my problem faster.
- **I strongly believe that everybody matters** and our differences make us unique and bring us together. 
 
On my quirky side
- I can go on tangets when trying to explain how something works. I love complex systems and sometimes forget that other people are not always that same. Just remind me to stay on topic if I am wasting time.
- I forget to eat very easily. This has been a bad habit since high school, but I am much better at managing it. 
- I can get very deep into a project and focus on it for hours on end, sometimes forgetting other things like what time it is or what my other tasks were. If you see blocks of focus time on my calendar, it's most likely to keep me focused on my time constraints.
- My dog really loves to cuddle and will occasionally hop into my lap to relax. It may happen in a meeting and it is totally okay to stare at her goofy face!

<br>

##### Inspired by

I read too many ReadMe's to list them all individually. This [ones from the UX Research manager](https://gitlab.com/alasch/about-anne) served me greatly though and are recommended.  

